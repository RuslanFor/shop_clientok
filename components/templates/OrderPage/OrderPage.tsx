import { useStore } from 'effector-react'
import { toast } from 'react-toastify'
import { useRouter } from 'next/router'
import { MutableRefObject, useEffect, useRef, useState } from 'react'
import {
  $shoppingCart,
  $totalPrice,
  setShoppingCart,
} from '@/context/shopping-cart'
import { formatPrice } from '@/utils/common'
import OrderAccordion from '@/components/modules/OrderPage/OrderAccordion'
import { $mode } from '@/context/mode'
import { checkPaymentFx, makePaymentFx } from '@/app/api/payment'
import { removeFromCartFx } from '@/app/api/shopping-cart'
import { $user, $userCity } from '@/context/user'
import styles from '@/styles/order/index.module.scss'
import spinnerStyles from '@/styles/spinner/index.module.scss'
import stylesForm from '@/styles/feedbackForm/index.module.scss'
import emailjs from '@emailjs/browser'
import NameInputOrder from '@/components/modules/FeedbackFormOrder/NameInput'
import PhoneInputOrder from '@/components/modules/FeedbackFormOrder/PhoneInput'
import EmailInputOrder from '@/components/modules/FeedbackFormOrder/EmailInput'
import { useForm } from 'react-hook-form'
import { FeedbackInputs } from '@/types/feedbackForm'
import CartInputOrder from '@/components/modules/FeedbackFormOrder/CartInput'


const OrderPage = () => {
  const mode = useStore($mode)
  const user = useStore($user)
  const userCity = useStore($userCity)
  const shoppingCart = useStore($shoppingCart)
  const totalPrice = useStore($totalPrice)
  const darkModeClass = mode === 'dark' ? `${styles.dark_mode}` : ''
  const [orderIsReady, setOrderIsReady] = useState(false)
  const [agreement, setAgreement] = useState(false)
  const router = useRouter()
  
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FeedbackInputs>()
  const [spinner, setSpinner] = useState(false)
  const formRef = useRef() as MutableRefObject<HTMLFormElement>

  const submitForm = () => {
    setSpinner(true)
    emailjs
      .sendForm(
        'service_aj26ogw',
        'template_j0lmlem',
        formRef.current,
        '-3lrSGTLT7wCy7J1r'
      )
      .then((result) => {
        setSpinner(false)
        setOrderIsReady(false)
        toast.success(`Сообщение отправлено! ${result.text}`)
        resetCart()
      })
      .catch((error) => {
        setSpinner(false)
        toast.error(`Что-то пошло не так! ${error.text}`)
      })

    formRef.current.reset()
  }






  const handleAgreementChange = () => setAgreement(!agreement)

  


  const resetCart = async () => {
    await removeFromCartFx(`/shopping-cart/all/${user.userId}`)
    setShoppingCart([])
  }

  return (
    <section className={styles.order}>
      <div className="container">
        <h2 className={`${styles.order__title} ${darkModeClass} `}>
          Оформление заказа
        </h2>
        <div className={styles.order__inner}>
          
          <div className={styles.order__cart}>
            
            <OrderAccordion
              setOrderIsReady={setOrderIsReady}
              showDoneIcon={orderIsReady}
            />
          </div>
          
          <div className={styles.order__pay}>
            
            
            <div className={`${styles.feedback_form} ${darkModeClass}`}>
              
              <form
                ref={formRef}
                className={stylesForm.feedback_form__form}
                onSubmit={handleSubmit(submitForm)}
              >
                <NameInputOrder
                  register={register}
                  errors={errors}
                  darkModeClass={darkModeClass}
                />
                <PhoneInputOrder
                  register={register}
                  errors={errors}
                  darkModeClass={darkModeClass}
                />
                <EmailInputOrder
                  register={register}
                  errors={errors}
                  darkModeClass={darkModeClass}
                />
                <CartInputOrder
                  
                />
                
                
              </form>
            </div>
            <h3 className={`${styles.order__pay__title} ${darkModeClass}`}>
              Итого
            </h3>
            <div className={`${styles.order__pay__inner} ${darkModeClass}`}>
              <div className={styles.order__pay__goods}>
                <span>
                  Товары (
                  {shoppingCart.reduce(
                    (defaultCount, item) => defaultCount + item.count,
                    0
                  )}
                  )
                </span>
                <span>{formatPrice(totalPrice)} P</span>
              </div>
              <div className={styles.order__pay__total}>
                <span>На сумму</span>
                <span className={darkModeClass}>
                  {formatPrice(totalPrice)} P
                </span>
              </div>
              <button
                disabled={!(orderIsReady && agreement)}
                className={styles.order__pay__btn}
                onClick={handleSubmit(submitForm)}
              >
                {spinner ? (
                  <span
                    className={spinnerStyles.spinner}
                    style={{ top: '6px', left: '47%' }}
                  />
                ) : (
                  'Подтвердить заказ'
                )}
              </button>
              <label
                className={`${styles.order__pay__rights} ${darkModeClass}`}
              >
                <input
                  className={styles.order__pay__rights__input}
                  type="checkbox"
                  onChange={handleAgreementChange}
                  checked={agreement}
                />
                <span className={styles.order__pay__rights__text}>
                  <strong>Согласен с условиями</strong> Правил пользования
                  торговой площадкой и правилами возврата
                </span>
              </label>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default OrderPage