import { useStore } from "effector-react/compat"
import { useState } from 'react';
import { $mode } from '@/context/mode';
import styles from '@/styles/catalog/index.module.scss'
import { IBoilerPart } from "@/types/boilerparts";
import Link from "next/link";
import { formatPrice } from "@/utils/common";
import { $shoppingCart } from "@/context/shopping-cart";
import CartHoverSvg from "@/components/elements/CartHoverSvg/CartHoverSvg";
import CartHoverCheckedSvg from "@/components/elements/CartHoverCheckedSvg/CartHoverCheckedSvg";
import spinnerStyles from '@/styles/spinner/index.module.scss'
import { toggleCartItem } from "@/utils/shopping-cart";
import { $user } from '@/context/user';
import { toast } from "react-toastify";
import useRedirectByUserCheck from "@/hooks/useRedirectByUserCheck";
import { useRouter } from "next/router";

const CatalogItem = ({ item }: { item: IBoilerPart }) => {
    const mode = useStore($mode)
    const user = useStore($user)
    const shoppingCart = useStore($shoppingCart)
    const isInCart = shoppingCart.some((cartItem) => cartItem.partId === item.id)
    const [spinner,setSpinner] = useState(false)
    const darkModeClass = mode === 'dark' ? `${styles.dark_mode}` : ''
    const { shouldLoadContent } = useRedirectByUserCheck()
    const router = useRouter()
    
    const toggleToCart = () => {
        if(shouldLoadContent){
            toggleCartItem(user.username, item.id, isInCart)
        }
        else{
            toast.error('Нужно авторизироваться')
            router.push('/auth')
        }
            
        
            
        
        
    }

    return(
        <li className={`${styles.catalog__list__item} ${darkModeClass}`}>
             <Link href={`/catalog/${item.id}`} passHref legacyBehavior>
            <img src={JSON.parse(item.images)[0]} alt={item.name} />
            </Link>
            <div className={styles.catalog__list__item__inner}>
                <Link href={`/catalog/${item.id}`} passHref legacyBehavior>
                    <h3 className={styles.catalog__list__item__title}>{item.name}</h3>
                </Link>
                <span className={`${styles.catalog__list__item__code}  ${darkModeClass}`}>Артикул: {item.vendor_code}</span>
                <span className={styles.catalog__list__item__price}>{formatPrice(item.price)} P</span>
            </div>
            <button className={`${styles.catalog__list__item__cart} ${isInCart ? styles.added : ''}`} disabled={spinner} onClick={toggleToCart }>
                {spinner ? (
                <div className={spinnerStyles.spinner} style={{top:6,left:6}}/>
                ) : (
                <span>{isInCart ? <CartHoverCheckedSvg/> : <CartHoverSvg/>}</span>
                )}
            </button>
        </li>
    )
}

export default CatalogItem;