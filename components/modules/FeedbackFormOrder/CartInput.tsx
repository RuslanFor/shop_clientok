import { IFeedbackInput } from '@/types/feedbackForm'
import styles from '@/styles/feedbackForm/index.module.scss'
import { useStore } from 'effector-react'
import { $userCity, $user } from '@/context/user';
import { useEffect, useState } from 'react';
import { $shoppingCart } from '@/context/shopping-cart';
import { $mode } from '@/context/mode';



const CartInputOrder = () => {
    const mode = useStore($mode)
    const darkModeClass = mode === 'dark' ? `${styles.dark_mode}` : ''
 
  const  shopingCart = useStore($shoppingCart)


  const [shopping, setShopping] = useState<{ name: string; vendor_code: string; count: number; }[]>([]);


useEffect(() => {
  setShopping(shopingCart.map(e => ({ name: e.name,vendor_code: e.vendor_code, count: e.count })));
}, [shopingCart]);

  console.log(shopping)


  

  return(
  <label  style={{opacity:'0'}}     >
    
    <input
      className={`${styles.feedback_form__form__input}`}
      type='text'
      defaultValue={shopping.map(e => `(${e.name}:${e.vendor_code} x${e.count})`).join(", ")}
      value={shopping.map(e => `(${e.name}{${e.vendor_code}} x${e.count})`).join(", ")}
      style={{cursor:'default'}}
      name='cart'
      
      
    />
    
  </label>)}


export default CartInputOrder