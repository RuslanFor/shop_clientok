import { IFeedbackInput } from '@/types/feedbackForm'
import styles from '@/styles/feedbackForm/index.module.scss'
import { $mode } from '@/context/mode';
import { useStore } from 'effector-react';

const NameInputOrder = ({ register, errors}: IFeedbackInput) => {
  const mode = useStore($mode)
    const darkModeClass = mode === 'dark' ? `${styles.dark_mode}` : ''
  return (
    <label className={`${styles.feedback_form__form__label} ${darkModeClass}`}>
      <span>Имя</span>
      <input
        className={styles.feedback_form__form__input}
        type="text"
        placeholder="Иван"
        {...register('name', {
          required: 'Введите Имя!',
          pattern: {
            value: /^[а-яА-Яa-zA-ZёЁ]*$/,
            message: 'Недопустимое значение',
          },
          minLength: 2,
          maxLength: 15,
        })}
      />
      {errors.name && (
        <span className={styles.error_alert}>{errors.name?.message}</span>
      )}
      {errors.name && errors.name.type === 'minLength' && (
        <span className={styles.error_alert}>Минимум 2 символа!</span>
      )}
      {errors.name && errors.name.type === 'maxLength' && (
        <span className={styles.error_alert}>Не более 15 символов!</span>
      )}
    </label>
  )
}

export default NameInputOrder