import { IFeedbackInput } from '@/types/feedbackForm'
import styles from '@/styles/feedbackForm/index.module.scss'
import { useStore } from 'effector-react'
import { $mode } from '@/context/mode';


const EmailInputOrder = ({ register, errors }: IFeedbackInput) => {

    const mode = useStore($mode)
    const darkModeClass = mode === 'dark' ? `${styles.dark_mode}` : ''

    return(
      <label className={`${styles.feedback_form__form__label} ${darkModeClass}`}>
      <span>Email</span>
      <input
        className={styles.feedback_form__form__input}
        type="email"
        placeholder="ivan@gmail.com"
        {...register('email', {
          required: 'Введите Email!',
          pattern: {
            value: /\S+@\S+\.\S+/,
            message: 'Неправильный Email!',
          },
        })}
      />
      {errors.email && (
        <span className={styles.error_alert}>{errors.email?.message}</span>
      )}
    </label>
    )}

  


export default EmailInputOrder